# Windows7 Setup

## Requirements:
[Powershell 4.0](https://www.microsoft.com/en-ca/download/details.aspx?id=40855) This will not work on 2.0 I.e the default installation.


## What it does?
###### This automates the setup of a standard Windows 7 PC

- Disable and hide Telemetry and Bloat/Windows10 Focused updates
- Install [SPICE](https://www.spice-space.org/) Only when under QEMU system
- Install [Chocolatey](https://chocolatey.org/)
- Install [Firefox ESR](https://chocolatey.org/packages/FirefoxESR)
- Install [7zip](https://chocolatey.org/packages/7zip.install)
- Install [Sublime 3](https://chocolatey.org/packages/SublimeText3)
- Install [MobaXTerm](https://chocolatey.org/packages/MobaXTerm)
- Install [Fiddler](https://chocolatey.org/packages/fiddler)
- Install [x64dbg](https://chocolatey.org/packages/x64dbg.portable)
- Install [Windbg](https://chocolatey.org/packages/windbg)
- Install [dnSpy](https://chocolatey.org/packages/dnspy)
- Install [Visual Studio](https://chocolatey.org/packages/visualstudio2017-installer)
- Install [HxD](https://chocolatey.org/packages/HxD)


- Revert to classic theme
- Disable Services based on BlackViper Tweaked Settings

## Updates Disabled and hidden;

'KB2505438', 'KB2670838', 'KB2882822', 'KB2952664',
'KB2976978', 'KB2976987', 'KB2977759', 'KB2990214', 'KB3012973',
'KB3015249', 'KB3021917', 'KB3022345', 'KB3035583', 'KB3042058',
'KB3044374', 'KB3050267', 'KB3064683', 'KB3065987', 'KB3065988',
'KB3068707', 'KB971033', 'KB454826', 'KB3150513', 'KB3148198','KB3068708'
'KB3146449', 'KB3138612', 'KB3135449', 'KB3135445', 'KB3123862','KB3074677',
'KB3118401', 'KB3112343', 'KB3112336', 'KB3107998', 'KB3093983','KB3072318',
'KB3102810', 'KB3102812', 'KB3090045', 'KB3088195', 'KB3086255','KB3068708',
'KB3083711', 'KB3083710', 'KB3083325', 'KB3083324', 'KB3081954','KB3022345',
'KB3081454', 'KB3081437', 'KB3080149', 'KB3075853','KB3075851', 'KB3075249',
'KB3075249', 'KB3080149', 'KB3112343', 'KB3135445', 'KB3021917', 'KB3035583',
'KB2990214', 'KB2952664', 'KB3065987', 'KB3050265', 'KB2976987', 'KB2902907',
'KB3102810', 'KB3123862', 'KB3081954', 'KB3139929', 'KB3138612', 'KB3150513',
'KB3133977', 'KB3139923', 'KB3173040', 'KB3146449', 'KB3044374', 'KB2976978',
'KB3012973', 'KB2977759', 'KB971033', 'KB3065988', 'KB3083325', 'KB3083324',
'KB3075853','KB3050267', 'KB3075851', 'KB3046480', 'KB3083710', 'KB3083711',
'KB3112336','KB3103696','KB3121260', 'KB3099834', 'KB3084905', 'KB3029606',
'KB3134815', 'KB3139921','KB3126030', 'KB3060746','KB3102429', 'KB3092627',
'KB3080800', 'KB3078405', 'KB3080042', 'KB3103699','KB3100956', 'KB3054464',
'KB3061493', 'KB3133924', 'KB3060793', 'KB3063843','KB3072019', 'KB3126033',
'KB3095701', 'KB3056347','KB3100919', 'KB3123242', 'KB3079318', 'KB3018467',
'KB3013791', 'KB3078676','KB3059316', 'KB3045746','KB3128650', 'KB3055343',
'KB3118401', 'KB3071663','KB3091297', 'KB3096433','KB3045634', 'KB3065013',
'KB3125210', 'KB3029603', 'KB3029432', 'KB3053863','KB3112148', 'KB3064059',
'KB3060383', 'KB3087041','KB3121255', 'KB3054256', 'KB3107998', 'KB3121261',
'KB3041857', 'KB3087137','KB3132080', 'KB3130896','KB3058168', 'KB3055323'
